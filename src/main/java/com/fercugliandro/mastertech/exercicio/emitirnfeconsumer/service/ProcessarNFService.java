package com.fercugliandro.mastertech.exercicio.emitirnfeconsumer.service;

import com.fercugliandro.mastertech.exercicio.emitirnf.model.EmissaoNFE;

public interface ProcessarNFService {

    void processarNF(EmissaoNFE nfe);

}
