package com.fercugliandro.mastertech.exercicio.emitirnfeconsumer.client;

import com.fercugliandro.mastertech.exercicio.emitirnfeconsumer.client.dto.CreateNFERequest;
import com.fercugliandro.mastertech.exercicio.emitirnfeconsumer.oauth.configuration.OAuth2FeignConfiguration;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

@FeignClient(value = "emitirnfe", configuration = OAuth2FeignConfiguration.class)
public interface EmissaoNfeClient {

    @PostMapping("/nfe/gerarnfe/")
    void gerarNfe(@RequestBody CreateNFERequest request);
}
