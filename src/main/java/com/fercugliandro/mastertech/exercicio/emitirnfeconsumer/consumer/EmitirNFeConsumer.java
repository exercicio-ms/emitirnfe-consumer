package com.fercugliandro.mastertech.exercicio.emitirnfeconsumer.consumer;

import com.fercugliandro.mastertech.exercicio.emitirnf.model.EmissaoNFE;
import com.fercugliandro.mastertech.exercicio.emitirnfeconsumer.service.ProcessarNFService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class EmitirNFeConsumer {

    @Autowired
    private ProcessarNFService service;

    @KafkaListener(topics = "luis-biro-0", groupId = "cugli-1")
    public void receberNFe(@Payload EmissaoNFE nfe) {

        service.processarNF(nfe);

    }
}
