package com.fercugliandro.mastertech.exercicio.emitirnfeconsumer.service.impl;

import com.fercugliandro.mastertech.exercicio.emitirnf.model.EmissaoNFE;
import com.fercugliandro.mastertech.exercicio.emitirnfeconsumer.client.EmissaoNfeClient;
import com.fercugliandro.mastertech.exercicio.emitirnfeconsumer.client.EmpresaClient;
import com.fercugliandro.mastertech.exercicio.emitirnfeconsumer.client.dto.CreateNFERequest;
import com.fercugliandro.mastertech.exercicio.emitirnfeconsumer.model.Empresa;
import com.fercugliandro.mastertech.exercicio.emitirnfeconsumer.service.ProcessarNFService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProcessarNFServiceImpl implements ProcessarNFService {

    private static final Double ALIQUOTA_IMPOSTO_IRRF = 0.015;
    private static final Double ALIQUOTA_IMPOSTO_CSLL  = 0.3;
    private static final Double ALIQUOTA_IMPOSTO_COFINS = 0.065;


    @Autowired
    private EmpresaClient empresaClient;

    @Autowired
    private EmissaoNfeClient emissaoNfeClient;


    @Override
    public void processarNF(EmissaoNFE nfe) {

        CreateNFERequest request = null;
        if (!isPessoaJuridica(nfe)){
            request = calculoValoresNFePessoaFisica(nfe);
        } else {
            request = calculoValoresNFePessoaJuridica(nfe);
        }

        emissaoNfeClient.gerarNfe(request);
    }

    private Empresa buscarEmpresa(String cnpj) {
        return  empresaClient.buscaDadosCnpj(cnpj);
    }

    private boolean isPessoaJuridica(EmissaoNFE nfe) {
        return nfe.getIdentidade().length() > 11;
    }

    private boolean validarOptanteSimplesNacional(Empresa empresa) {
        Double capitalSocialPadrao = Double.parseDouble("1000000.00");
        Double capitalSocialEmpresa = Double.parseDouble(empresa.getCapital_social());

        return capitalSocialEmpresa >= capitalSocialPadrao;
    }

    private CreateNFERequest calculoValoresNFePessoaFisica(EmissaoNFE nfe) {
        CreateNFERequest request = new CreateNFERequest();
        request.setIdEmissaoNFe(nfe.getId());

        request.setValorInicial(nfe.getValor());
        request.setValorFinal(nfe.getValor());

        return request;
    }

    private CreateNFERequest calculoValoresNFePessoaJuridica(EmissaoNFE nfe) {

        Empresa empresa = buscarEmpresa(nfe.getIdentidade());
        boolean isOptanteSN = validarOptanteSimplesNacional(empresa);

        CreateNFERequest request = new CreateNFERequest();
        request.setIdEmissaoNFe(nfe.getId());
        request.setValorInicial(nfe.getValor());

        if (!isOptanteSN) {
            request.setValorIRRF(calcularIRRF(nfe.getValor()));
            request.setValorFinal(nfe.getValor() - request.getValorIRRF());
        } else {
            request.setValorIRRF(calcularIRRF(nfe.getValor()));
            request.setValorCSLL(calcularCSLL(nfe.getValor()));
            request.setValorCofins(calcularCOFINS(nfe.getValor()));

            request.setValorFinal(nfe.getValor() - request.getValorIRRF() - request.getValorCSLL() - request.getValorCofins());
        }

        return request;
    }

    private Double calcularIRRF(Double valorInicial) {
        return valorInicial.doubleValue() * ALIQUOTA_IMPOSTO_IRRF;
    }

    private Double calcularCSLL(Double valorInicial) {
        return valorInicial.doubleValue() * ALIQUOTA_IMPOSTO_CSLL;
    }

    private Double calcularCOFINS(Double valorInicial) {
        return valorInicial.doubleValue() * ALIQUOTA_IMPOSTO_COFINS;
    }
}
