package com.fercugliandro.mastertech.exercicio.emitirnfeconsumer.client;

import com.fercugliandro.mastertech.exercicio.emitirnfeconsumer.model.Empresa;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "cpnjclient", url = "http://www.receitaws.com.br/v1/cnpj")
public interface EmpresaClient {

    @GetMapping(value={"/{cnpj}"} )
    Empresa buscaDadosCnpj(@PathVariable(value = "cnpj") String cnpj);
}
