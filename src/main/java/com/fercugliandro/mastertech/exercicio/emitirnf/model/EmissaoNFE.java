package com.fercugliandro.mastertech.exercicio.emitirnf.model;

public class EmissaoNFE {

    private Long id;
    private String identidade;
    private Double valor;
    private StatusNFE status;
    private Long idNfe;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getIdentidade() {
        return identidade;
    }

    public void setIdentidade(String identidade) {
        this.identidade = identidade;
    }

    public Double getValor() {
        return valor;
    }

    public void setValor(Double valor) {
        this.valor = valor;
    }

    public StatusNFE getStatus() {
        return status;
    }

    public void setStatus(StatusNFE status) {
        this.status = status;
    }

    public Long getIdNfe() {
        return idNfe;
    }

    public void setIdNfe(Long idNfe) {
        this.idNfe = idNfe;
    }
}
