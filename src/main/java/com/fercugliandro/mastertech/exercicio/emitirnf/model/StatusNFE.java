package com.fercugliandro.mastertech.exercicio.emitirnf.model;

public enum StatusNFE {

    PENDING("pending"),
    COMPLETE("complete");

    private String status;

    StatusNFE(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public StatusNFE get(String status) {
        if (status.equalsIgnoreCase(PENDING.status))
            return StatusNFE.PENDING;
        else
            return StatusNFE.COMPLETE;
    }
}
